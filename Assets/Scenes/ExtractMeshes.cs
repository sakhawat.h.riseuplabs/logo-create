using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

public class ExtractMeshes : MonoBehaviour
{
    public GameObject fbxGameObject;
    // public string fbxFilePath;
    public string saveFolder = "Assets/ExtractedMeshes/";

    void Start()
    {
        if (fbxGameObject == null)
        {
            Debug.LogError("FBX GameObject is not assigned!");
            return;
        }

        // Load the FBX file
        MeshFilter[] meshFilters = fbxGameObject.GetComponentsInChildren<MeshFilter>();

        if (meshFilters.Length == 0)
        {
            Debug.LogError("No meshes found in the FBX GameObject!");
            return;
        }
        
        saveFolder = "Assets/ExtractedMeshes/New/";

        if (!Directory.Exists(saveFolder))
        {
            Directory.CreateDirectory(saveFolder);
        }
        
        foreach (MeshFilter meshFilter in meshFilters)
        {
            Mesh mesh = meshFilter.sharedMesh;
            if (mesh != null)
            {
                string meshName = mesh.name + ".asset";
                string assetPath = saveFolder + meshName;

                // Create a new GameObject for the mesh
                GameObject newGameObject = new GameObject(mesh.name);
                
                // Set the position of the new GameObject to the bottom of the mesh
                newGameObject.transform.position = fbxGameObject.transform.position - (mesh.bounds.size.y * fbxGameObject.transform.lossyScale.y / 2) * Vector3.up;

                // Make the mesh a child of the new GameObject
                MeshFilter newMeshFilter = newGameObject.AddComponent<MeshFilter>();
                newMeshFilter.sharedMesh = Instantiate(mesh);
                newGameObject.AddComponent<MeshRenderer>();

                // Save the mesh as an asset
                AssetDatabase.CreateAsset(newMeshFilter.sharedMesh, assetPath);
            }
        }

        Debug.Log("Meshes extracted and saved to Assets/ExtractedMeshes/New folder.");
        
        
        
        
        
        
        
        


        /*foreach (MeshFilter meshFilter in meshFilters)
        {
            Mesh mesh = Instantiate(meshFilter.sharedMesh); // Create a copy of the mesh

            // Create a unique name for the mesh based on its hierarchy
            string meshName = mesh.name.Replace("(Clone)", "");
            Transform parent = meshFilter.transform.parent;
            while (parent != null)
            {
                meshName = parent.name[..6] + "_" + meshName;
                parent = parent.parent;
            }

            // Save the mesh asset
            string path = saveFolder + meshName + ".asset";
            AssetDatabase.CreateAsset(mesh, path);
        }*/

        // Refresh the Asset Database to make sure the new assets are visible in the Project window
        AssetDatabase.Refresh();
    }
}