mergeInto(LibraryManager.library, {

    UploadSingleFile: function (fileTypePTR, WebGLGameObjectNamePTR, WebGLMethodPTR) {
        var fileType = Pointer_stringify(fileTypePTR);
        var WebGLGameObjectName = Pointer_stringify(WebGLGameObjectNamePTR);
        var WebGLMethod = Pointer_stringify(WebGLMethodPTR);

        var fileInput = document.createElement('input');
        fileInput.setAttribute('id', WebGLGameObjectName);
        fileInput.setAttribute('type', 'file');
        fileInput.setAttribute('style', 'display:none;');
        fileInput.setAttribute('style', 'visibility:hidden;');
        fileInput.setAttribute('accept', fileType);

        this.value = null;
        fileInput.onchange = function (event) {
            var selectedFile = {
                fileName: event.target.files[0].name,
                blobURL: URL.createObjectURL(event.target.files[0])
            };
            console.log(selectedFile);

            // File selected
            SendMessage(WebGLGameObjectName, WebGLMethod, JSON.stringify(selectedFile));

            // Remove after file selected
            document.body.removeChild(fileInput);
        }
        document.body.appendChild(fileInput);
        fileInput.click();
    },

    DownloadSingleFile: function (fileTypePTR, fileNamePTR, byteArray, byteArraySize, WebGLGameObjectNamePTR, WebGLMethodPTR) {
        var gameObjectName = Pointer_stringify(WebGLGameObjectNamePTR);
        var methodName = Pointer_stringify(WebGLMethodPTR);
        var fileType = Pointer_stringify(fileTypePTR);
        var fileName = Pointer_stringify(fileNamePTR);

        var bytes = new Uint8Array(byteArraySize);
        for (var i = 0; i < byteArraySize; i++) {
            bytes[i] = HEAPU8[byteArray + i];
        }

        var downloader = document.createElement('a');
        downloader.setAttribute('id', gameObjectName);
        downloader.href = URL.createObjectURL(new Blob([bytes], { type: 'application/octet-stream' }));
        downloader.download = fileName+fileType;
        document.body.appendChild(downloader);
        downloader.click();
        document.body.removeChild(downloader);
        SendMessage(gameObjectName, methodName);
    },
});