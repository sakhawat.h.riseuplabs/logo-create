﻿using System.Runtime.InteropServices;

namespace Walid.System
{
    public static class JavascriptManager
    {
        [DllImport("__Internal")]
        public static extern void UploadSingleFile(string fileTypePTR, string WebGLGameObjectNamePTR, string WebGLMethodPTR);
        [DllImport("__Internal")]
        public static extern void DownloadSingleFile(string fileTypePTR, string fileNamePTR, byte[] byteArray, int byteArraySize, string WebGLGameObjectNamePTR, string WebGLMethodPTR);
    }
}