﻿using System.IO;
using UnityEngine;
using System.Collections;
using Walid.System.IO;
using System;
using UnityEditor;

public enum ScreenResolution
{
    nHD,
    HD,
    FHD,
    QHD,
    UHD4K,
    LogoHD,
    LogoUHD,
    LogoUHD8K,
}

/*public enum FileType
{
    zip,
    gif,
    csv,
    jpg,
    png,
    txt,
    json,
    pdf,
    mp3,
    wav,
    docx,
    pptx,
    doc,
    ppt
}*/
public class ScreenShot : MonoBehaviour
{
    public Camera SnapshotCamera;
    public ScreenResolution photoResolution;
    public GameObject waterMarkCanvas;
    //public Texture2D watermark;
    //public float waterMarkRGBAlpha;
    private int width, height;

    public void Snapshot(bool isTransparent)
    {
        StartCoroutine(DeActivateWatermark(isTransparent));
    }
    
    private IEnumerator DeActivateWatermark(bool isTransparent)
    {
        //GameManager.Singleton.progressBar.barObject.SetActive(true);
        //GameManager.Singleton.progressBar.loadingText.text = string.Format("Taking Snapshot");
        // SnapshotCamera.gameObject.SetActive(true); moved to Snapshot(bool isTransparent) Button Action
        //SnapshotCamera.gameObject.SetActive(true);
        if (waterMarkCanvas != null)
        {
            waterMarkCanvas.SetActive(true);
        }
        yield return new WaitForSeconds(2f);
        GetResolution();
        if (isTransparent)
        {
            TransparentShot();
        }
        else
        {
            SimpleShot();
        }
        if (waterMarkCanvas != null)
        {
            waterMarkCanvas.SetActive(false);
        }
        SnapshotCamera.gameObject.SetActive(false);
        //GameManager.Singleton.progressBar.barObject.SetActive(false);
    }   

    private void SimpleShot()
    {
        Texture2D texture = new Texture2D(width, height, TextureFormat.ARGB32, false);
        Rect grab_area = new Rect(0, 0, width, height);
        // This code will generate error to Reconfigure Resolution of Snapshot
        //SnapshotCamera.targetTexture.width = width;
        //SnapshotCamera.targetTexture.height = height;
        // This code will generate error to Reconfigure Resolution of Snapshot
        RenderTexture.active = SnapshotCamera.targetTexture;
        texture.ReadPixels(grab_area, 0, 0);
        SnapshotCamera.Render();
        texture.Apply();
        //AddWaterMark(texture);
        byte[] bytes = ImageConversion.EncodeToJPG(texture);
        // FileBrowser.Singleton.DownloadFile(fileType: FileType.jpg, fileName: string.Format("{0}-{1}", "DjangoAPIManager.Singleton.project.name", DateTime.Now.ToString("hh-mm-ss-dd-mm-yyyy")), bytes: bytes);
        string fileName = "Screenshot_" + System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".png";
        string filePath = Application.dataPath + "/Screenshots/" + fileName; // You can change the directory as per your requirement
        File.WriteAllBytes(filePath, bytes);

        // Import the screenshot to Unity's asset database (optional)
        AssetDatabase.ImportAsset("Assets/Screenshots/" + fileName);
    }

    private void TransparentShot()
    {
        Texture2D textureTransparent = new Texture2D(width, height, TextureFormat.ARGB32, false);
        Texture2D textureWhite = new Texture2D(width, height, TextureFormat.ARGB32, false);
        Texture2D textureBlack = new Texture2D(width, height, TextureFormat.ARGB32, false);
        Rect grab_area = new Rect(0, 0, width, height);
        SnapshotCamera.targetTexture.width = width;
        SnapshotCamera.targetTexture.height = height;
        RenderTexture.active = SnapshotCamera.targetTexture;
        SnapshotCamera.Render();
        SnapshotCamera.clearFlags = CameraClearFlags.SolidColor;
        SnapshotCamera.backgroundColor = Color.black;
        SnapshotCamera.Render();
        textureBlack.ReadPixels(grab_area, 0, 0);
        textureBlack.Apply();
        SnapshotCamera.backgroundColor = Color.white;
        SnapshotCamera.Render();
        textureWhite.ReadPixels(grab_area, 0, 0);
        textureWhite.Apply();
        // Create Alpha from the difference between black and white camera renders
        for (int y = 0; y < textureTransparent.height; ++y)
        {
            for (int x = 0; x < textureTransparent.width; ++x)
            {
                float alpha = textureWhite.GetPixel(x, y).r - textureBlack.GetPixel(x, y).r;
                alpha = 1.0f - alpha;
                Color color;
                if (alpha == 0)
                {
                    color = Color.clear;
                }
                else
                {
                    color = textureBlack.GetPixel(x, y) / alpha;
                }
                color.a = alpha;
                textureTransparent.SetPixel(x, y, color);
            }
        }
        //AddWaterMark(textureTransparent);
        byte[] bytes = ImageConversion.EncodeToPNG(textureTransparent);
        // FileBrowser.Singleton.DownloadFile(fileType: FileType.png, fileName: string.Format("{0}", DateTime.Now.ToString("hh-mm-ss-dd-mm-yyyy")), bytes: bytes);
        string fileName = "Screenshot_" + System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".png";
        string filePath = Application.dataPath + "/Screenshots/" + fileName; // You can change the directory as per your requirement
        File.WriteAllBytes(filePath, bytes);

        // Import the screenshot to Unity's asset database (optional)
        AssetDatabase.ImportAsset("Assets/Screenshots/" + fileName);
    }

    private void GetResolution()
    {
        Action action = photoResolution switch
        {
            ScreenResolution.nHD => delegate () { width = 640; height = 360; },
            ScreenResolution.HD => delegate () { width = 1366; height = 768; },
            ScreenResolution.FHD => delegate () { width = 1920; height = 1080; },
            ScreenResolution.QHD => delegate () { width = 2560; height = 1440; },
            ScreenResolution.UHD4K => delegate () { width = 3840; height = 2160; },
            ScreenResolution.LogoHD => delegate () { width = 2048; height = 2048; },
            ScreenResolution.LogoUHD => delegate () { width = 4096; height = 4096; },
            ScreenResolution.LogoUHD8K => delegate () { width = 8192; height = 8192; },
            _ => delegate () { width = 1920; height = 1080; }
        };
        action?.Invoke();
    }
}